package jettyLog;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

public class Logger {

	public static void log (HttpServletRequest request,int status){
		//1) make log String from request
		String remoteIp = request.getRemoteHost();
		//time
		Date now = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy hh:mm:ss");
		String ts = formatter.format(now);
		
		//String ts = "[" + now.getDay() + "/" + now.getMonth() + "/" + now.getYear() + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds()+"]";
		//method 
		String method = request.getMethod();
		//uri
		String url = request.getRequestURL().toString();
		String uri = request.getQueryString(); 
		//User-Agent
		String userAgent = request.getHeader("User-Agent");
		//2) print to Err
		String str = remoteIp + " - [" + ts + "] - " +  method + " - " + status + " - " + url+"?"+uri + " - " + userAgent;
		Logger.stdTx(str, status);
	}
	private static void stdTx(String str, int sc){
		if (sc != 200){
			System.err.println(str);
		} else {
			//access.log
			System.out.println(str);
		}
	}
	@SuppressWarnings("unused")
	private static void logPerStatusCode(String str,int sc){
		//read JSONed config with routes
		//if route not found, use standard destination or System.err;
	}
}
