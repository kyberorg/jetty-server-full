package net.virtajetty.conf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;

import net.virtajetty.conf.struct.ServerConf;
import net.virtajetty.conf.struct.VHostConf;

import com.google.gson.Gson;
import com.google.gson.stream.MalformedJsonException;

/**
 * Static class with parsers
 * 
 * @author Alex Muravya <asm@virtalab.net>
 *
 */
public class Parser {
	private static Gson gson = new Gson();
	
	/**
	 * parser for Server Config
	 *
	 * @return ServerConf Struct for Server Config 
	 * @throws FileNotFoundException when config not found
	 * @throws MalformedJsonException when config contain not valid JSON, that cannot be correctly parsed into Struct 
	 */
	public static ServerConf parseServerConf() throws FileNotFoundException, MalformedJsonException {
		//read
		FileReader file = new FileReader("conf/jetty.cnf");
		//parse
		ServerConf conf = gson.fromJson(file, ServerConf.class);
		//return
		return conf;
	}
	/**
	 * Parser for VHost Config
	 * 
	 * @param File vHostConfig config file
	 * @return VHostConf Struct for VHost
	 * @throws FileNotFoundException when config not found
	 * @throws MalformedJsonException when config contain not valid JSON, that cannot be correctly parsed into Struct 
	 */
	public static VHostConf parseVHost(File vHostConfig) throws FileNotFoundException, MalformedJsonException {
		//read
		FileReader file = new FileReader(vHostConfig);
		//parse
		VHostConf conf = gson.fromJson(file, VHostConf.class);
		//return
		return conf;
	}
	
	public static void parseLogConfiguration(HashMap<String, String> logs){}
}
