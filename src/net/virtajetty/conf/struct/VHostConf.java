package net.virtajetty.conf.struct;

import java.util.HashMap;

public class VHostConf {
	private String name;
	private int port;
	private String base_path;
	private String root;
	
	public String getRoot() {
		return root;
	}
	public String getBase_path() {
		return base_path;
	}
	private HashMap<String,String> logs;
	public String getName() {
		return name;
	}
	public int getPort() {
		return port;
	}
	public HashMap<String, String> getLogs() {
		return logs;
	}
}
