package net.virtajetty.conf.struct;

import java.util.HashMap;

public class ServerConf {
	private String pid;
	private String vhosts;
	private HashMap<String,String> headers;
	private HashMap<String,String> logs;
	
	public String getPid() {
		return pid;
	}
	public String getVhosts() {
		return vhosts;
	}
	public HashMap<String, String> getHeaders() {
		return headers;
	}
	public HashMap<String, String> getLogs() {
		return logs;
	}
}
