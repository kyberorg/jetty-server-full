package net.virtajetty;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.stream.MalformedJsonException;

import net.virtajetty.conf.Parser;
import net.virtajetty.conf.struct.ServerConf;
import net.virtajetty.conf.struct.VHostConf;

public class JettyServer {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		//init block
		ServerConf srvCnf = new ServerConf();
		VHostConf siteCnf = new VHostConf();
		int pid = 0;
		
		//parse server config
		try{
		srvCnf = Parser.parseServerConf();
		}catch(FileNotFoundException fnfe){
			//TODO handle
		}catch (MalformedJsonException mje) {
			//TODO handle
		}
		//get pid
		String rawPid =ManagementFactory.getRuntimeMXBean().getName();
		String strPid = rawPid.replaceAll("[^0-9 ]","");
		try{
			pid = Integer.parseInt(strPid);
		}catch(NumberFormatException nume){
			//TODO handle
		}
		if(pid == 0){
			//TODO report
			System.exit(1);
		}
		//write pid to pidFile
		String pidFile = srvCnf.getPid();
		
		try{
		  FileWriter file = new FileWriter(new File(pidFile));
		  file.write(pid);
		  file.close();
		}catch(FileNotFoundException fnfe){
			//TODO handle
		}catch(IOException io){
			//TODO handle
		}
		
		//read vhosts location
		String vHostsPath = srvCnf.getVhosts();
		
		ArrayList<File> sites = JettyServer.getSiteConfigs(vHostsPath);
		
		//for Site
		for (File site: sites){				
			//parse it
			try {
				siteCnf = Parser.parseVHost(site);
			} catch (FileNotFoundException e) {
				// TODO handle
			} catch (MalformedJsonException e) {
				// TODO handle
			}
			 //name
			String name = siteCnf.getName();
			  //TODO use name for logging
		     //port
			 int port = siteCnf.getPort();
			 //log (as hashMap to parse log)
			 HashMap<String,String> log = siteCnf.getLogs();
			 //site root
			 String siteRoot = siteCnf.getRoot();
			 //base path
			 String basePath = siteCnf.getBase_path();
			 
		  // add root+bin and root+lib to classpath 
		  JettyServer.modifyClassPath(siteRoot);
		  //create new server
		  
		  //create new context
		  
		 //read site conf (parse)
		 	//main package
		   //servlets (HashMap)
		
		  //for servlet
		  	//addServlet to server
			
		}//end of for Site loop	
	}
	
	/**
	 * Modifies class path by adding bin and site lib to class patrh
	 * @param root Site Root
	 */
	private static void modifyClassPath(String root){
		
	}
	
	/**
	 * Make list of valid configs
	 * 
	 * @param vHostsPath Path to VHosts
	 * @return File[] only-valid configs
	 */
	private static ArrayList<File> getSiteConfigs(String vHostsPath){
		File folder = new File(vHostsPath);
		File[] files = folder.listFiles();
		ArrayList<File> sites = new ArrayList<File>();
		
		//for Site
		for (int i =0; i < files.length; i++){
			File file = files[i];
			if(!(file.isFile())){
			String siteName = file.getName();
			if (siteName.endsWith(".cnf")||siteName.endsWith(".CNF")||siteName.endsWith(".conf")||siteName.endsWith(".CONF")){
				sites.add(file);
			}//end of "is siteName endsWith" if
			}//end of "isFile" if
		}
		//return
		return sites;
	}
}
