package jettySrv;

import java.io.FileNotFoundException;
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServlet;

import jettyConfig.Parser;
import jettyStruct.ServerCnfStruct;

import org.eclipse.jetty.server.Server;

import org.eclipse.jetty.servlet.*;

import com.google.gson.stream.MalformedJsonException;

public class JettyServer {
	public static void main(String[] args) throws Exception
    {
		//init
		int port = 0;
		StringBuffer siteRoot = new StringBuffer();
		try{
		  ServerCnfStruct conf = Parser.parseServerConfig();
		  //port (move per vhost conf)
		  //port = conf.getPort();
		  //if(port<=0 || port >= 65535){
		  //		throw new NumberFormatException();
		  //	}
		  
		  //pid
		  String rawPid=ManagementFactory.getRuntimeMXBean().getName();
		  String pid=rawPid.replaceAll("[^0-9 ]","");
		  System.out.println(pid);
		  
		}catch(FileNotFoundException fnfe){
			JettyServer.fatalLog("No config found. Make sure that you have jetty.cnf at conf folder");
		}catch(MalformedJsonException mje){
			
		}catch(NumberFormatException numEx){
			JettyServer.fatalLog("Server Port must be a Number from 1 to 65535");
		
		}catch(Exception e){
			e.printStackTrace();
			System.exit(1);
		}
				
		ConfStruct conf = new ConfStruct();
		try{
			conf = Parser.parseConfig(siteRoot);
		}catch(FileNotFoundException fileEx){
			JettyServer.fatalLog("No config found. Make sure that you have server.cnf at cnf folder");
		}catch(MalformedJsonException jsonEx){
			JettyServer.fatalLog("It seems like config found, but has no valid JSON. See documentation and examples at git repo");
		}catch(ClassCastException jsonEx){
			JettyServer.fatalLog("It seems like config found, but has no valid JSON. See documentation and examples at git repo");
		}catch(Exception e){
			System.err.println(e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
		
		String conText = conf.getContext();
		if(conText == null){
			System.err.println("Context is empty. Using /");
			conText = "/";
		}
		
		//Context
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        context.setContextPath(conText);

        //Context -> Servlet
        
        String pack = conf.getMainPkg();
        
        if(pack == null){
        	JettyServer.fatalLog("Main package is undefined. Without this I cannot find your Servlets. Currently we don't support 'default' package");
        }
        HashMap<String,String> servlets = conf.getServlets();
        if(servlets == null){
        	JettyServer.fatalLog("No servlets found. Exiting.");
        }
        
        Iterator<String> iter = servlets.keySet().iterator();
        
        while(iter.hasNext()){
        	String servletName = (String) iter.next();
        	String path = (String) servlets.get(servletName);
        	
        	//reflection
        	Class<?> srvlt = Class.forName(pack+"."+servletName);
        	HttpServlet servlet = (HttpServlet) srvlt.newInstance();
        	
        	context.addServlet(new ServletHolder(servlet),path);
        	
        }
        
        //context.addServlet(new ServletHolder(new JavaServlet()),"/*");
        //context.addServlet(new ServletHolder(new OneServlet()),"/one/*");
         
        //Context -> Server
        Server server = new Server(port);
        server.setHandler(context);
        //server run
        server.start();
        server.join();
    }

	
	private static void fatalLog(String mess){
		System.err.println(mess);
		System.exit(1);
	}


} 
