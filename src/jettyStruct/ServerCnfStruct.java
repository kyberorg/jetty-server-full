package jettyStruct;

import java.util.HashMap;

public class ServerCnfStruct {
	private int port;
	private String pid;
	private String vhosts;
	public int getPort() {
		return port;
	}
	public String getPid() {
		return pid;
	}
	public String getVhosts() {
		return vhosts;
	}
	public HashMap<String, String> getHeaders() {
		return headers;
	}
	private HashMap<String,String> headers;
}
