package jettyConfig;

import java.io.FileNotFoundException;
import java.io.FileReader;

import jettySrv.ConfStruct;
import jettyStruct.ServerCnfStruct;

import com.google.gson.Gson;
import com.google.gson.stream.MalformedJsonException;

public class Parser {
	private static Gson gson = new Gson();
	
	public static ConfStruct parseConfig(StringBuffer siteRoot) throws FileNotFoundException, MalformedJsonException{
		//read
		FileReader file = new FileReader(siteRoot.toString()+"/cnf/server.cnf");
		//parse
		ConfStruct conf = gson.fromJson(file, ConfStruct.class);
		//return
		return conf;
	}
	public static ServerCnfStruct parseServerConfig() throws FileNotFoundException, MalformedJsonException {
		//read
		FileReader cnf = new FileReader("conf/jetty.cnf");
		//parser
		ServerCnfStruct conf = gson.fromJson(cnf, ServerCnfStruct.class);
		//return
		return conf;
	}
}
